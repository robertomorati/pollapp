# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _


__author__ = 'Roberto Morati  <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2020 Poll App'
__version__ = '1.0.0'


class User(AbstractUser):
    username = models.CharField(max_length=50, blank=True, null=True)
    email = models.EmailField(_('email'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    def __str__(self):
        return "{}".format(self.email)


class Poll(models.Model):
    """
    shared is a poll that can be shared with friends
    public is a poll available to be voted by anyone
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user',null=True, blank=True)
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    shared = models.BooleanField(default=False)
    public = models.BooleanField(default=False)


class Choice(models.Model):
    poll = models.ForeignKey(Poll, related_name='choices', on_delete=models.CASCADE)
    choice = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    class Mete:
        unique_together = ['poll', 'choice']


class UserChoice(models.Model):
    choice = models.ForeignKey(Choice, on_delete=models.CASCADE)
    user = models.ForeignKey(User,null=True, blank=True, on_delete=models.CASCADE)