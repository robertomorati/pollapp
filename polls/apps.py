# -*- coding: utf-8 -*-
from django.apps import AppConfig

__author__ = 'Roberto Morati <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2020'
__version__ = '1.0.0'


class PollsConfig(AppConfig):
    name = 'polls'