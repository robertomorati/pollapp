# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations, models
from polls.models import User

def create_superuser(apps, schema_editor):
    User.objects.create_superuser(username='robertomorati', password='morati', email='robertomorati@gmail.com')

class Migration(migrations.Migration):

    dependencies = [('polls', '0001_initial'),]

    operations = [migrations.RunPython(create_superuser)]