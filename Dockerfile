# Set the base image to use to Ubuntu
FROM python:3.5

MAINTAINER  Roberto Morati <robertomorati@gmail.com>

# Local directory with project source
RUN mkdir -p /var/www/pollapp
ENV HOME=/var/www/pollapp


WORKDIR $HOME

# Update the default application repository sources list
RUN apt-get update && apt-get install -y \
        gettext-base \
        python3-venv \
        python3-pip \
        python3-dev

RUN apt-get install -y binutils libproj-dev gdal-bin nano

RUN pip3 install --upgrade pip
RUN pip install gunicorn

COPY requirements/requirements.txt $HOME
COPY manage.py $HOME

COPY entrypoint.sh $HOME
RUN chmod +x entrypoint.sh

RUN mkdir media static logs
VOLUME ["$HOME/media/", "$HOME/logs/","$HOME/static/"]

COPY . $HOME

RUN ls
# Install python dependencies
RUN pip install -r $HOME/requirements.txt