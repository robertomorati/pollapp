# -*- coding: utf-8 -*-
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


__author__ = 'Roberto Morati  <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2020 Poll App'
__version__ = '1.0.0'


class PollPagination(PageNumberPagination):
    page_size = 1000
    page_size_query_param = 'page_size'
    
    def get_paginated_response(self, data):

        url_next = None 
        if self.page.has_next():
            url_next = self.page.next_page_number()

        url_previous = None
        if self.page.has_previous():
            url_previous = self.page.previous_page_number()

        return Response({
            'links': {
               'next': url_next,
               'previous': url_previous,
            },
            'count': self.page.paginator.count,
            'total_pages': self.page.paginator.num_pages,
            'results': data
        })