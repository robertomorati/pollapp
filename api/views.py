# -*- coding: utf-8 -*-
from rest_framework import viewsets, generics
from rest_framework import mixins, status
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.decorators import action, detail_route
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly

from api.serializers import UserSerializer, PollSerializer, VotePollSerializer
from api.permissons import UserPermission, PollPermissions, PollObjectPermissions
from api.pagitation import PollPagination
from polls.models import User, Poll, Choice, UserChoice
from django.db import transaction

from rest_framework.decorators import action, detail_route
from rest_framework import permissions

__author__ = 'Roberto Morati  <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2020 Poll App'
__version__ = '1.0.0'


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (UserPermission,)

class PollViewSet(viewsets.ModelViewSet):
    """
    Allow create, update, list, delete and get 
    """
    queryset = Poll.objects.all()
    serializer_class = PollSerializer
    permission_classes = (PollPermissions,)

    def get_queryset(self):
        polls = super(PollViewSet, self).get_queryset()
        user = self.request.user
        if not user.is_anonymous:
            polls = Poll.objects.filter(user=user)
        else:
            polls = Poll.objects.all()
            
        return polls


class SharedPollViewSet(viewsets.ModelViewSet):
    """
    Retrives shared and public polls to vote
    """
    pagination_class = PollPagination
    queryset = Poll.objects.all()
    serializer_class = VotePollSerializer
    permission_classes = (PollObjectPermissions,)
    
    def get_queryset(self):
        polls =  super().get_queryset()
        user = self.request.user
        polls = []
        if not user.is_anonymous:
            polls = Poll.objects.filter(shared=True).exclude(choices__isnull=True)
        else:
            polls = Poll.objects.filter(shared=True, public=True).exclude(choices__isnull=True)
        return polls
    
    # action to vote in a choice
    @action(methods=['POST'], detail=False, url_path='add_vote/(?P<pk>\d+)',  permission_classes=[PollObjectPermissions], name='Vote')
    def add_vote(self, request, *args, **kwargs):
        
        # TODO: move to Choice model
        with transaction.atomic():
            c = Choice.objects.select_for_update().get(pk=kwargs['pk'])
            c.votes += 1
            c.save()
        
        if self.request.user.is_authenticated:
            UserChoice.objects.create(choice=c, user=self.request.user)
        else:
            UserChoice.objects.create(choice=c)

        return Response(['done'], status=status.HTTP_200_OK)

    # action to remove the vote in a choice
    @action(methods=['POST'], detail=False, url_path='remove_vote/(?P<pk>\d+)', name='Remove Vote')
    def remove_vote(self, request, *args, **kwargs):
        
        # TODO: move to Choice model
        with transaction.atomic():
            c = Choice.objects.select_for_update().get(pk=kwargs['pk'])
            if  c.votes > 0:
                c.votes -= 1
            c.save()

        if self.request.users.is_authenticated:
            UserChoice.objects.filter(choice=c, user=self.request.user).first().delete()
        else:
            UserChoice.objects.filter(choice=c).first().delete()

        return Response(['done'], status=status.HTTP_200_OK)

    # action to retrieves data to a chart
    @action(methods=['GET'], detail=False, url_path='get_data_poll/(?P<pk>\d+)', name='Retrieves Data Poll')
    def get_data_poll(self, request, *args, **kwargs):
        
        data = []
        poll = Poll.objects.prefetch_related('choices').filter(pk=kwargs['pk'])[0]

        data.append({'title' : poll.title })
        data.append({'description' : poll.description})
        
        choices = []
        for choice in poll.choices.all().order_by('votes'):
            choices.append({'choice' : choice.choice,'votes' : choice.votes  })

        return Response([{'data':data,'choices':choices}], status=status.HTTP_200_OK)
