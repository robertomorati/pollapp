# -*- coding: utf-8 -*-
from rest_framework import serializers, status
from polls.models import User, Poll, Choice

__author__ = 'Roberto Morati <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2020'
__version__ = '1.0.0'


class UserSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        return user

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
        instance.save()
        
        return instance


class ChoiceSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Choice
        fields = ('pk','choice',)
        read_only_fields = ('pk',)


class ChoiceCustomField(serializers.StringRelatedField):
    def to_internal_value(self, data):
        return data

    def to_representation(self, value):
        choice = { 
            'pk': value.pk, 
            'choice': value.choice 
            } 
        return choice


class PollSerializer(serializers.ModelSerializer):
    choices =  ChoiceCustomField(many=True)

    class Meta:
        model = Poll
        fields = ('pk', 'title', 'description', 'shared', 'public','choices')
        read_only_fields = ('pk',)

    def create(self, validated_data):

        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

            choices = validated_data.pop('choices')
            poll = Poll.objects.create(user=user, **validated_data)
            for choice in choices:
                Choice.objects.create(poll=poll, choice=choice['choice'])

        return poll
       
    def update(self, instance, validated_data):
        
        choices = validated_data.pop('choices')
        instance.title =  validated_data.pop('title')
        instance.description =  validated_data.pop('description')
        instance.shared =  validated_data.pop('shared')
        instance.public =  validated_data.pop('public')
        instance.save()

        choices_pk = []
        for choice in choices:
            if 'pk' in choice:
                choices_pk.append(choice['pk'])
                Choice.objects.filter(pk=choice['pk']).update(choice=choice['choice'])
            else:
                c = Choice.objects.create(poll=instance, choice=choice['choice'])
                c.save()
                choices_pk.append(c.pk)

        # delete choices for poll
        Choice.objects.filter(poll__pk=instance.pk).exclude(pk__in=choices_pk).delete()

        return instance


class VotePollSerializer(serializers.ModelSerializer):
    choices =  ChoiceCustomField(many=True)

    class Meta:
        model = Poll
        fields = ('pk', 'title', 'description','choices')
        read_only_fields = ('pk','title', 'description')