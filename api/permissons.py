# -*- coding: utf-8 -*-
from rest_framework import permissions

__author__ = 'Roberto Morati  <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2020 Poll App'
__version__ = '1.0.0'


class UserPermission(permissions.BasePermission):
    """
    Custom permission user
    """
    def has_permission(self, request, view):
        if view.action == 'list':
            return True
        elif view.action == 'create':
            return True
        elif view.action in ['update', 'partial_update', 'destroy']:
            return request.user.is_authenticated



class PollPermissions(permissions.BasePermission):
    """
    Custom permission poll
    """
    #def has_object_permission(self, request, view, obj):
    #    if view.action in ['partial_update']:
    #        return request.user.is_authenticated

    def has_permission(self, request, view):
        if view.action == 'list':
            return request.user.is_authenticated
        elif view.action in ['create','retrieve', 'update', 'partial_update', 'destroy']:
            return request.user.is_authenticated
        else:
            return request.user.is_authenticated


class PollObjectPermissions(permissions.BasePermission):
    """
    Custom permission object poll
    """
    def has_object_permission(self, request, view, obj):
        if obj.shared and obj.public and (view.action in ['retrieve', 'update', 'partial_update',]):
            return  True
        else:
            return request.user.is_authenticated

