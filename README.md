# Roberto Morati robertomorati.com

    Authentication Credentials
    
    username: robertomorati@gmail.com   password: morati
	

	Future enhancements:
       1 - Create GraphQL API with Graphene
       2 - Setup API Test
       2 - Improve documentation with Apiary
       3 - Use the userchoice to improve the Chart.
       4 - Allow create a Poll without user account. 
       5 - Use redux, although the workflow is simple. 

    I decided to allow any user to vote many times in the same Poll. This decision is based on the purpose tow allow tests. 


## 1. API
	
	The API can be used by the: http://localhost:8005/

    API Token:
    http://localhost:8005/api/doc/
    

    Takes a set of user credentials and returns an access and refresh JSON web token pair to prove the authentication of those credentials. 
    http://localhost:8005/api/token/              


    Takes a refresh type JSON web token and returns an access type JSON web token if the refresh token is valid.
    http://localhost:8005/api/token/refresh/
    

    Takes a token and indicates if it is valid. This view provides no information about a token�s fitness for a particular use.
    http://localhost:8005/api/token/verify/


## 2. Clone the project

	git clone git clone git@bitbucket.org:robertomorati/pollapp.git


## 3. Docker

	docker-compose build --force-rm
	docker-compose up -d


## 4 Last step, the React App

    Clone the React project, install the (npm install) package.json and run npm start
    git clone https://bitbucket.org/robertomorati/poll-app-react.git

    The baseURL for Axios was defined in services/api.js

    PollApp:  http://localhost:3000/