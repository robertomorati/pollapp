"""pollapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from api.views import UserViewSet, PollViewSet, SharedPollViewSet
from rest_framework_swagger.views import get_swagger_view


from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)

schema_view = get_swagger_view(title='Poll App API')

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'polls', PollViewSet)
router.register(r'votes', SharedPollViewSet)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(router.urls)),
    url(r'api/doc/', schema_view), 
    # url('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    url('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    url('api/token/verify/', TokenVerifyView.as_view(), name='token_verify'),
]
